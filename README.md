# README #

Content Management System Access Checker 'cmsack' is a utility that makes requests to known file and folder structures of popular CMS platforms such as drupal, wordpress, etc. It will establish a connection to each location and file and report back what pages it can access, which ones exist but cannot access, and which ones do not exist. It can help penetration testers and redteamers identify incorrect file permissions and access to such files/folders. Currently drupal and wordpress are supported, In the future I will push other open source free cms lists..etc.

### Quick Overview ###

* Python 2.7
* Requires python libraries: requests, urllib3, argparse
* Successfully audit CMS permissions (Currently Drupal and Wordpress Only)



### Install ###

* Git or clone the directory
* install the python libraries found in requirements.txt using pip
* execute the script by using ./cmsACK.py or python cmsACK.py. Pass the required flags to the program for it to run properly. example: python cmsACK.py -u http://example.com -l drupal.txt -s mysessionname
* Required flags are -u (url) -l (cms list you want to check) -s (session name)
* Optional flags are -t (time) -q (url que/pool) example of using all flags: example: python cmsACK.py -u http://example.com -l drupal.txt -s mysessionname -q 10 -t 10



### Usage In-Depth ###

REQUIRED FLAGS
* user@linuxterminal: python cmsACK.py -u http://example.com -l drupal.txt -s mysession

The above usage tells cmsACK the base URL 'http://example.com', do not add a trailing '/' at the end, leave it off. If we know that our example website is using drupal, then we want to use the drupal.txt list by using -l drupal.txt. We also need to provide a session name as cmsACK will save the results to a txt files when it completes. We do this by using -s sessionname

OPTIONAL FLAGS
* -q and -t

These flags are optional, You can limit the time and processing que of sending requests if you are trying to be more stealthy. "-q 2 -t 1" would mean only process 2 urls at a time and then wait 1 second.
