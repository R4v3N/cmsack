#!/usr/bin/python2

import requests
import argparse
import time

from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry

parser = argparse.ArgumentParser(description='Drupal Access Checker')
parser.add_argument('-u','--url', help='URL, do not add trailing / character. Example use: http://example.com', required=True)
parser.add_argument('-l','--list', help='list that contains filetree structure', required=True)
parser.add_argument('-s', '--session', help='session name', required=True)
parser.add_argument('-t', '--time', help='Adjust Timing To Prevent Connection Errors in seconds', default=0, type=int, required=False)
parser.add_argument('-q', '--que', help='Adjust The URL Que Before Timing Adjustments', default=10, type=int, required=False)
args = parser.parse_args()


starturl = str(args.url)
startlist = str(args.list)
startsession = str(args.session)
starttime = args.time
startque = args.que


headers = {'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:64.0) Gecko/20100101 Firefox/64.0'}


class bcolors:
	HEADER = '\033[95m'
	OKBLUE = '\033[94m'
	OKGREEN = '\033[92m'
	WARNING = '\033[93m'
	FAIL = '\033[91m'
	ENDC = '\033[0m'
	BOLD = '\033[1m'
	UNDERLINE = '\033[4m'

def query():
		f = open(startlist)
		data = [line.strip() for line in open(startlist)]
		newlist = []
		oklist = []
		redirectlist = []
		notfoundlist = []
		forbiddenlist = []
		for item in data:
			fullpath = starturl+item
			newlist.append(fullpath)
		print '[*] Built '+str(len(newlist))+' queries...'
		print '[*] Checking '+starturl+' for valid response...'
		response = requests.get(starturl, headers=headers)
		print '[*] '+'['+str(response.status_code)+']'+' PATH: --> '+starturl
		count = 1
		status = 1
		for url in newlist:
			try:
				newresponse = requests.get(url, headers=headers, timeout=5)
			except requests.exceptions.HTTPError:
				print '[*] HTTP Error...'
				pass
			except requests.exceptions.ConnectionError:
				print '[*] Connection Error..'
				pass
			except Exception as e:
				print e
				continue
			if newresponse.status_code == 200:
				oklist.append(url)
				message = bcolors.OKGREEN +'['+str(status)+':'+str(len(newlist))+']'+ '['+str(newresponse.status_code)+']'+' PATH: --> '+url+ bcolors.ENDC
				print message
			if (newresponse.status_code > 300 and newresponse.status_code < 403):
				redirectlist.append(url)
				message = bcolors.WARNING + '['+str(status)+':'+str(len(newlist))+']'+ '['+str(newresponse.status_code)+']'+' PATH: --> '+url + bcolors.ENDC
				print message

			if newresponse.status_code == 403:
				forbiddenlist.append(url)
				message = bcolors.OKBLUE + '['+str(status)+':'+str(len(newlist))+']'+ '['+str(newresponse.status_code)+']'+' PATH: --> '+url+ bcolors.ENDC
				print message

			if newresponse.status_code == 404:
				notfoundlist.append(url)
				message = bcolors.FAIL + '['+str(status)+':'+str(len(newlist))+'] '+'['+str(newresponse.status_code)+']'+' PATH: --> '+url + bcolors.ENDC
				print message
			count = count+1
			status = status+1
			if count > startque:
				print '[*] Allowing Catch Up...'
				time.sleep(starttime)
				count = 0


		print ''
		print '[*] All Done..'
		print '[*] Generating Statistics..'
		oklen = str(len(oklist))
		relen = str(len(redirectlist))
		folen = str(len(forbiddenlist))
		nolen = str(len(notfoundlist))
		print '[*] ORIGINAL QUERY COUNT: '+str(len(newlist))

		print '[*] HTTP OK: '+oklen
		for item in oklist:
			print item

		print '[*] REDIRECT: '+relen
		for item in redirectlist:
			print item

		print '[*] FORBIDDEN: '+folen
		for item in forbiddenlist:
			print item

		print '[*] NOT FOUND: '+nolen
		for item in notfoundlist:
			print item

		with open(startsession+'.OK.txt', 'a+') as f:
			for item in oklist:
				f.write(item+'\n')

		with open(startsession+'.RE.txt', 'a+') as f:
			for item in redirectlist:
				f.write(item+'\n')
		with open(startsession + '.FO.txt', 'a+') as f:
			for item in forbiddenlist:
				f.write(item+'\n')
		with open(startsession + '.NF.txt', 'a+') as f:
			for item in notfoundlist:
				f.write(item+'\n')

		print '[*] session files saved...'

def banner():
	print '''
 ____ ____ ____ ____ ____ ____ 
||c |||m |||s |||A |||C |||K ||
||__|||__|||__|||__|||__|||__||
|/__\|/__\|/__\|/__\|/__\|/__\|
CMS Access Checker v 1.0
R4v3N - https://bitbucket.org/R4v3N
	'''
	query()


if __name__ == '__main__':
	banner()
